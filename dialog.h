#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QStringList>
#include <QStringListModel>
#include "sshmemclient.h"

#if QT_VERSION < 0x040800
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QGroupBox>
#include <QListView>
#include <QFormLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QSpinBox>
#include <QTimer>
#else
QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
class QListView;
class QVBoxLayout;
class QHBoxLayout;
class QComboBox;
class QTreeView;
class QTreeWidget;
class QTreeWidgetItem;
class QFileDialog;
class QSpinBox;
class QTimer;

class SSHMemClient;
QT_END_NAMESPACE
#endif


typedef struct {
    int                        client_id;
    /*  for futher usage  */
} cmb_element_t;
Q_DECLARE_METATYPE(cmb_element_t)



class Dialog : public QDialog {
    Q_OBJECT

public:
    Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
/*  Message Queue status  */
    void PeerInitialized (QString msg_queue_name);
    void PeerConnectionError (int ret);
    void PeerPingReceived (qint64 sourceID, int line);
/*  Message Queue Commands  */
    void MQPingClient ();
/*  SharedMemory Commands  */
    void ShMemFlashAll ();
    void ShMemDumpAll ();
/*  SharedMemory Status  */
    void SharedMemoryAttached (QString key, int size);
    void SharedMemoryAttachError (int err);
/*  Socket Commands  */
    void ClientConnect ();
    void ClientDisconnect();
/*  Socket events  */
    void NewSocketMessage (QString msg);
    void SocketInitialized (qintptr sock_id, int client_id);
    void NewClientConnected (qintptr sock_id, int client_id);
    void ConnectionUpdate ();
    void ClientListItemSelect (int idx);
/*  Event polling system management  */
    void polling_procedure ();


private:
    SSHMemClient  *sshm_client;

    cmb_element_t selected_element;

/*  Graphics functions  */
    /*  first panel  */
    void g_createFGBoxSocketFileName ();
    void g_createHGBoxSocketConn ();
    void g_createFGBoxSharedMemoryCommand();
    void g_createFGBoxMessageQueueCommand();
    void g_setSocketStatus ();

    /*  second panel  */
    void g_setSystemView ();

    /*  third panel  */
    void g_setLogger ();

    /*  updater functions  */
    void update_status ();
    void addLog (QString str);
    void update_client_view ();
    void update_peer_view (cmb_element_t data);

    /*  Event polling system management  */
    bool              use_polling;
    QTimer            *polling_timer;

/*  Graphics Elements  */
    QHBoxLayout       *mainLayout;
    QVBoxLayout       *first_mainLayout;
    QVBoxLayout       *second_mainLayout;
    QVBoxLayout       *third_mainLayout;

    /*  first panel  */
    enum { NumGridRows = 3, NumButtonsConn = 2 };
    QString           str_pbn_SocketConn;
    QStringList       str_list;

    QGroupBox         *fgb_SocketFileName;
    QLabel            *lbl_SocketFileName;
    QLineEdit         *txt_SocketFileName;

    QGroupBox         *hgb_SocketConn;
    QPushButton       *pbn_SocketConn[NumButtonsConn];

    QGroupBox         *fgb_ShMemCommands;
    QPushButton       *pbn_ShMemFlashAll;
    QPushButton       *pbn_ShMemDumpAll;

    QGroupBox         *fgb_MQCommands;
    QPushButton       *pbn_MQPingClient;
    QComboBox         *cmb_clientList;
    QSpinBox          *spb_MQPingIntLine;

    QGroupBox         *fgb_SocketStatus;
    QLabel            *lbl_status_conn;
    QLabel            *lbl_status_conn_info;

    /*  second panel  */
    QGroupBox         *vgb_systemStructure;
    QLabel            *lbl_localParameters;
    QLabel            *lbl_clientId;
    QLabel            *lbl_clientSockId;
    QLabel            *lbl_shmKey;
    QLabel            *lbl_shmSize;
    QLabel            *lbl_mqName;

    /*  third panel  */
    QGroupBox         *vgb_Logging;
    QListView         *ltv_Logging;
    QStringList       list;
    QStringListModel  *model;
};

#endif // DIALOG_H
