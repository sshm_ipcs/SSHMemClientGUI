#-------------------------------------------------
#
# Project created by QtCreator 2016-02-04T22:19:23
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
lessThan(QT_MAJOR_VERSION, 5): QT += widget

TARGET = SSHMemClientGUI
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../qtcreator_compiled//release/ -lshmIPCS
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../qtcreator_compiled/debug/ -lshmIPCS
else:unix: LIBS += -L$$PWD/../qtcreator_compiled/ -lshmIPCS -lrt


INCLUDEPATH += $$PWD/../shmIPCS
DEPENDPATH += $$PWD/../shmIPCS
