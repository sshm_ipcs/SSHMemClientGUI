#if QT_VERSION >= 0x040800
#include <QtWidgets>
#else
#include <QWidget>
#endif

#include "dialog.h"

Dialog::Dialog(QWidget *parent) : QDialog(parent) {

    this->use_polling = true;

    str_pbn_SocketConn = "Connect, Disconnect";

    g_createFGBoxSocketFileName ();
    g_createHGBoxSocketConn ();
    g_createFGBoxSharedMemoryCommand ();
    g_createFGBoxMessageQueueCommand ();
    g_setSocketStatus ();

    g_setSystemView ();

    g_setLogger ();


    first_mainLayout = new QVBoxLayout ();
    second_mainLayout = new QVBoxLayout ();
    third_mainLayout = new QVBoxLayout ();

    mainLayout = new QHBoxLayout ();

    first_mainLayout->addWidget (fgb_SocketFileName);
    first_mainLayout->addWidget (hgb_SocketConn);
    first_mainLayout->addWidget (fgb_ShMemCommands);
    first_mainLayout->addWidget (fgb_MQCommands);
    first_mainLayout->addWidget (fgb_SocketStatus);

    second_mainLayout->addWidget (vgb_systemStructure);

    third_mainLayout->addWidget (vgb_Logging);

    mainLayout->addLayout (first_mainLayout);
    mainLayout->addLayout (second_mainLayout);
    mainLayout->addLayout (third_mainLayout);

    setLayout(mainLayout);
    setWindowTitle(tr("Simnow Shared Memory CLIENT GUI"));

    connect (pbn_SocketConn[0], SIGNAL(clicked(bool)),
            this, SLOT(ClientConnect()));
    connect (pbn_SocketConn[1], SIGNAL(clicked(bool)),
            this, SLOT(ClientDisconnect()));

    connect (pbn_ShMemFlashAll, SIGNAL(clicked(bool)),
             this, SLOT(ShMemFlashAll()));
    connect (pbn_ShMemDumpAll, SIGNAL(clicked(bool)),
             this, SLOT(ShMemDumpAll()));

    connect (pbn_MQPingClient, SIGNAL(clicked(bool)),
             this, SLOT(MQPingClient()));

    connect (cmb_clientList, SIGNAL(activated(int)),
             this, SLOT(ClientListItemSelect(int)));

    qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
    qRegisterMetaType<qintptr>("qintptr");

    this->sshm_client = new SSHMemClient (0, this->use_polling == true ? 1 : 0);
    if ( this->sshm_client != NULL ) {

        if ( this->use_polling == false ) {
            connect (sshm_client, SIGNAL(newMessage(QString)),
                     this, SLOT(NewSocketMessage(QString)));

            connect (sshm_client, SIGNAL(clientInitialized(qintptr,int)),
                     this, SLOT(SocketInitialized(qintptr,int)), Qt::QueuedConnection);

            connect (sshm_client, SIGNAL(clientPeerInitialized(QString)),
                     this, SLOT(PeerInitialized(QString)), Qt::QueuedConnection);

            connect (sshm_client, SIGNAL(clientPeerConnectionError(int)),
                     this, SLOT(PeerConnectionError(int)), Qt::QueuedConnection);

            connect (sshm_client, SIGNAL(PingReceived(qint64,int)),
                     this, SLOT(PeerPingReceived(qint64,int)));

            connect (sshm_client, SIGNAL(newClientConnected(qintptr,int)),
                     this, SLOT(NewClientConnected(qintptr,int)), Qt::QueuedConnection);

            connect (sshm_client, SIGNAL(clientUpdateInterconnection()),
                     this, SLOT(ConnectionUpdate()), Qt::QueuedConnection);

            connect (sshm_client, SIGNAL(sharedMemoryAttached(QString,int)),
                     this, SLOT(SharedMemoryAttached(QString,int)), Qt::QueuedConnection);

            connect (sshm_client, SIGNAL(sharedMemoryAttachError(int)),
                     this, SLOT(SharedMemoryAttachError(int)), Qt::QueuedConnection);
        } else {
            this->polling_timer = new QTimer();
            this->polling_timer->setInterval (500);
            this->polling_timer->setSingleShot (false);

            connect (polling_timer, SIGNAL(timeout()),
                     this, SLOT(polling_procedure()));

            this->polling_timer->start();
        }

        this->sshm_client->setSocketFileName ("sshmem");
        txt_SocketFileName->setText (this->sshm_client->getSocketFileName());
        update_status ();
    } else {
        lbl_status_conn->setText (tr("Client error"));
    }

    this->sshm_client->start();
}


Dialog::~Dialog()
{
    this->ClientDisconnect ();
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                             GUI SETTING                           |
 * |___________________________________________________________________|
 */
void Dialog::g_createFGBoxSocketFileName () {
    fgb_SocketFileName = new QGroupBox(tr(""));
    lbl_SocketFileName = new QLabel (tr("Socket File Name"));
    txt_SocketFileName = new QLineEdit ();
    QFormLayout *layout = new QFormLayout;

    layout->addRow (lbl_SocketFileName, txt_SocketFileName);
    fgb_SocketFileName->setLayout (layout);
}


void Dialog::g_createHGBoxSocketConn () {
    hgb_SocketConn = new QGroupBox(tr("Socket Commands"));
    QHBoxLayout *layout = new QHBoxLayout;

    this->str_list << this->str_pbn_SocketConn.split(",");

    for ( int i = 0 ; i < NumButtonsConn ; ++i ) {
        pbn_SocketConn[i] = new QPushButton(this->str_list.at(i));
        layout->addWidget (pbn_SocketConn[i]);
    }
    hgb_SocketConn->setLayout (layout);
}


void Dialog::g_createFGBoxSharedMemoryCommand () {
    QGridLayout *layout = new QGridLayout ();
    fgb_ShMemCommands   = new QGroupBox (tr("SharedMemory Commands"));

    pbn_ShMemFlashAll   = new QPushButton (tr("Flash All"));
    pbn_ShMemDumpAll    = new QPushButton (tr("Dump All"));

    layout->addWidget (pbn_ShMemFlashAll, 0, 0, 1, 1);
    layout->addWidget (pbn_ShMemDumpAll,  0, 1, 1, 1);
    fgb_ShMemCommands->setLayout (layout);
}


void Dialog::g_createFGBoxMessageQueueCommand () {
    QGridLayout *layout = new QGridLayout ();
    fgb_MQCommands      = new QGroupBox (tr("MQ Commands"));

    pbn_MQPingClient    = new QPushButton (tr("Ping Client"));
    cmb_clientList      = new QComboBox ();
    spb_MQPingIntLine   = new QSpinBox ();

    spb_MQPingIntLine->setRange (MIN_INT_LINE, MAX_INT_LINE);
    spb_MQPingIntLine->setSingleStep (1);
    spb_MQPingIntLine->setPrefix ("interrupt ");

    layout->addWidget (pbn_MQPingClient,   0, 0, 1, 1);
    layout->addWidget (cmb_clientList,     0, 1, 1, 1);
    layout->addWidget (spb_MQPingIntLine,  0, 2, 1, 1);

    fgb_MQCommands->setLayout (layout);
}


void Dialog::g_setSocketStatus () {
    QFormLayout *layout  = new QFormLayout;
    fgb_SocketStatus     = new QGroupBox (tr("Socket Status"));

    lbl_status_conn      = new QLabel (tr("Conn. status"));
    lbl_status_conn_info = new QLabel ();

    layout->addRow (lbl_status_conn, lbl_status_conn_info);

    fgb_SocketStatus->setLayout (layout);
}


void Dialog::g_setSystemView () {
    QGridLayout *layout = new QGridLayout ();
    vgb_systemStructure = new QGroupBox (tr("Client Info"));

    lbl_localParameters = new QLabel (tr("Local Parameters:"));
    lbl_clientId        = new QLabel (tr("Client ID:"));
    lbl_clientSockId    = new QLabel (tr("Socked ID:"));
    lbl_shmKey          = new QLabel (tr("Shm key:"));
    lbl_shmSize         = new QLabel (tr("Shm size:"));
    lbl_mqName          = new QLabel (tr("MQ name:"));

    layout->addWidget (lbl_localParameters, 0, 0, 1, 1);
    layout->addWidget (lbl_clientId,        1, 0, 1, 1);
    layout->addWidget (lbl_clientSockId,    2, 0, 1, 1);
    layout->addWidget (lbl_shmKey,          3, 0, 1, 1);
    layout->addWidget (lbl_shmSize,         4, 0, 1, 1);
    layout->addWidget (lbl_mqName,          5, 0, 1, 1);

    vgb_systemStructure->setLayout (layout);
}


void Dialog::g_setLogger () {
    vgb_Logging = new QGroupBox ();
    QHBoxLayout *layout = new QHBoxLayout;
    ltv_Logging = new QListView ();
    layout->addWidget (ltv_Logging);
    vgb_Logging->setLayout (layout);

    model = new QStringListModel (this);
    ltv_Logging->setModel (model);
    ltv_Logging->setEditTriggers (QAbstractItemView::NoEditTriggers);
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                                                                   |
 * |___________________________________________________________________|
 */
void Dialog::update_status () {
    QString msg;
    sshMemClientPeer local = this->sshm_client->getLocalPeer ();
    if ( this->sshm_client->isClientConnected () ) {

        lbl_status_conn->setText (tr("Connected"));

        if ( this->sshm_client->isInitialized () ) {
            msg.sprintf ("Client ID: %llu", this->sshm_client->getClientID());
            lbl_clientId->setText (msg);

            msg.sprintf ("Socket ID: %llu", this->sshm_client->getSockID());
            lbl_clientSockId->setText (msg);

            msg = tr("MQ name: ") + local.message_port->get_fd_name ();
            lbl_mqName ->setText (msg);

            if ( this->sshm_client->isShmInitialized () ) {
                msg =  tr("Shm key: ") + this->sshm_client->getMemKey();
                lbl_shmKey->setText (msg);

                msg.sprintf ("Shm size: %llu", this->sshm_client->getMemSize()/1024);
                lbl_shmSize->setText (msg);

            }
        }

        this->pbn_SocketConn[0]->setEnabled (false);
        this->pbn_SocketConn[1]->setEnabled (true);

        this->pbn_ShMemFlashAll->setEnabled (true);
        this->pbn_ShMemDumpAll->setEnabled (true);

    } else {

        lbl_status_conn->setText (tr("Disconnected"));
        lbl_clientId->setText (tr("Client ID:"));
        lbl_clientSockId->setText (tr("Socket ID:"));
        lbl_shmKey->setText (tr("Shm key:"));
        lbl_shmSize->setText (tr("Shm size:"));
        lbl_mqName->setText (tr("MQ name:"));

        this->pbn_SocketConn[0]->setEnabled (true);
        this->pbn_SocketConn[1]->setEnabled (false);

        this->pbn_ShMemFlashAll->setEnabled (false);
        this->pbn_ShMemDumpAll->setEnabled (false);
    }
}

void Dialog::addLog (QString str) {
    list << str;
    model->setStringList(list);
}


void Dialog::update_client_view () {
    SSHMemPeerClient peerList = this->sshm_client->getRemotePeerList ();

    cmb_clientList->clear ();

    SSHMemPeerClient::iterator iter;
    for ( iter = peerList.begin() ; iter != peerList.end() ; ++iter) {
        QVariant var_client;
        cmb_element_t ce_client;
        QString str_client;
        str_client.sprintf ("Client %llu", iter->id);
        ce_client.client_id = iter->id;
        var_client.setValue (ce_client);
        cmb_clientList->addItem (str_client, var_client);
    }
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                             SOCKET SLOTS                          |
 * |___________________________________________________________________|
 */
void Dialog::ClientConnect () {
    this->sshm_client->ConnectClient ();
    update_status ();
}


void Dialog::ClientDisconnect () {
    this->sshm_client->DisconnectClient ();
    update_status ();
}


void Dialog::NewSocketMessage (QString msg) {
    this->addLog (msg);
}


void Dialog::SocketInitialized (qintptr sock_id, int client_id) {
    QString msg;

    msg.sprintf ("Own ID %d and sock ID %llu", client_id, sock_id);

    this->addLog (msg);
}


void Dialog::NewClientConnected (qintptr sock_id, int client_id) {
    QString msg;
    update_client_view ();
    msg.sprintf ("New Client with ID %d and sock ID %llu",
                 client_id, sock_id);

    this->addLog (msg);
}


void Dialog::ConnectionUpdate () {
    update_client_view ();
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                         SHARED MEMORY SLOTS                       |
 * |___________________________________________________________________|
 */
void Dialog::SharedMemoryAttached (QString key, int size) {
    QString msg;

    msg.sprintf ("SharedMemory attached with:");
    this->addLog (msg);
    msg =  tr("Shm key: ") + this->sshm_client->getMemKey();
    this->addLog (msg);
    msg.sprintf ("size: %dKB", size/1024);
    this->addLog (msg);
    update_status ();
}


void Dialog::SharedMemoryAttachError (int err) {
    this->addLog (this->sshm_client->getMsgFromErrCode (err));
}


void Dialog::ShMemFlashAll () {
    QString fileName = QFileDialog::getOpenFileName(0, QString(), QString(),
                                        tr("binary (*.bin)"));

    this->sshm_client->flash_memory_from_file (fileName);
}


void Dialog::ShMemDumpAll () {
    QString fileName = QFileDialog::getSaveFileName(0, QString(), QString(),
                                        tr("binary (*.bin)"));

    this->sshm_client->dump_memory_to_file (fileName);
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                   MESSAGE QUEUE SLOTS / COMMANDS                  |
 * |___________________________________________________________________|
 */
void Dialog::PeerInitialized (QString msg_queue_name) {
    QString msg;

    msg.sprintf ("Peer initialized");
    this->addLog (msg);
    msg = tr("Connected to MQ: ") + msg_queue_name;
    update_status ();
}


void Dialog::PeerConnectionError (int ret) {
    QString msg;
    msg.sprintf ("Peer error: %d", ret);
    this->addLog (msg);
}


void Dialog::PeerPingReceived (qint64 sourceID, int line) {
    QString msg;
    msg.sprintf ("Received ping from ID %d at line: %d",
                 sourceID, line);
    this->addLog (msg);
}


void Dialog::MQPingClient () {
    int interrupt = this->spb_MQPingIntLine->value();
    int clientID  = this->selected_element.client_id;
    int ret       = this->sshm_client->send_PingToClient (clientID, interrupt);
    if ( ret == 0 )
        this->addLog (tr("Ping send"));
    else
        this->addLog (tr("Error on sending ping"));
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                       COMBOBOX CLIENT SLOTS                       |
 * |___________________________________________________________________|
 */
void Dialog::ClientListItemSelect (int idx) {
    cmb_element_t ce;
    const QVariant var = cmb_clientList->itemData (idx, Qt::UserRole);
    if ( var.canConvert<cmb_element_t> () ) {
        ce = var.value<cmb_element_t> ();
        this->selected_element = ce;
    }
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                   EVENT POLLING SYSTEM MANAGEMENT                 |
 * |___________________________________________________________________|
 */
void Dialog::polling_procedure () {
    int                 n_msg, n_sys;
    queue_sys_element_t sys_msg;
    queue_msg_element_t client_msg;
    QString             msg;

    /*
     * The assumption is that the system can analyze only one
     *  message and one system notificaty at each tick clock
     */

    n_msg = this->sshm_client->num_pending_message ();
    if ( n_msg) {
        if ( this->sshm_client->pop_message (&client_msg) ) {
            PeerPingReceived (client_msg.source_id, client_msg.line);
        }
    }

    n_sys = this->sshm_client->num_pending_sys_msg ();
    if ( n_sys ) {
        if ( this->sshm_client->pop_sys_msg (&sys_msg) ) {

            switch (sys_msg.code) {
            case MSG_P_SOCK_INIT:
                SocketInitialized ((qintptr)sys_msg.data.at(0).toInt(),
                                   sys_msg.data.at(1).toInt());
                break;
            case MSG_P_SOCK_MQ_INFO:
                PeerInitialized (sys_msg.data.at(0).toString());
                break;
            case MSG_P_SOCK_ERROR:

                break;
            case MSG_P_SOCK_ADD_CLIENT:
                ConnectionUpdate ();
                break;
            case MSG_P_SOCK_NEW_CLIENT:
                NewClientConnected ((qintptr)sys_msg.data.at(0).toInt(),
                                    sys_msg.data.at(1).toInt());
                break;
            case MSG_P_SHMEM_KEY:
                SharedMemoryAttached (sys_msg.data.at(0).toString(),
                                      sys_msg.data.at(1).toInt());
                break;
                case MSG_P_MEM_ATTACH_ERROR:
                    SharedMemoryAttachError (sys_msg.data.at(0).toInt());
                    break;
            default:
                msg.sprintf ("Unknow msg sys code: %d", sys_msg.code);
                this->addLog (msg);
            }

        }
    }

}
